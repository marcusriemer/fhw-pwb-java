# Das Projekt sollte auf folgende Art organisiert werden:
# * Im Wurzelverzeichnis muss ein Makefile liegen.

# Im Makefile muss immer das default-Ziel “all” enthalten sein, das zur 
# Erzeugung des Programms verwendet wird. Sollte das Programm in einer
# interpretierten Sprache geschrieben werden, so ist hier eine leere Aktion
# einzutragen.
all:
	ant jar
	
# Zusätzlich ist ein Ziel “run” im Makefile zu definieren, um das Programm
# korrekt auszuführen. Sollten noch weitere Dateien für den Aufruf des Programms
# benötigt werden, so sind alle diese Daten in ein direktes Unterverzeichnis zu 
# speichern. Zugriffe auf diese Dateien müssen über relative Pfadnamen 
# realisiert werden, damit ein Verschieben in ein Installationsverzeichnis
# möglich wird. Je nach Aufruf im Makefile ist darauf zu achten, dass das
# Programm auch als “ausführbar” eingecheckt ist.	
run:
	@java -jar dist/pwb-ss15.jar

# Zu jeder Lösung gehört ein Projektname. Um den Namen des Projekts festzulegen,
# soll im Makefile auch noch ein Ziel “name” definiert werden. Mit make name
# wird dann der Projektname auf die Standardausgabe geschrieben. Der Projektname
# darf aus genau einer Zeile mit maximal 30 Ascii-Zeichen bestehen.	
name:
	@echo "Some Java Project"

# Nicht gefordert, aber praktisch	
clean:
	ant clean
