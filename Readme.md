# Java programs using the Netbeans IDE for a programming competition at the Unversity of Applied Sciences Wedel

Skeleton to use the Netbeans provided ant file with a makefile. Please consult the (german) regulations for the [competition held in the summer of 2015](http://samegame.asta-wedel.de/). You may download the project [directly from Bitbucket](https://bitbucket.org/marcusriemer/fhw-pwb-java/get/master.zip).

To set the correct `ANT_HOME` when using the windows machines at the computer pool simply execute the following command prior to `ant` or `make`:

    set ANT_HOME=C:\Program Files\apache-ant-1.9.3\