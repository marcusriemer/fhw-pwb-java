package org.fhw.pwb.ss15;

/**
 * Stub class to demonstrate a valid response for the programming competition
 * of the University of Applied Sciences Wedel held in the summer of 2015.
 * 
 * @author mri
 */
public class Main {
    /**
     * The programs receive all input via stdin and print their solutions to
     * stdout. Arguments via commandline are not used by the actual competition.
     * 
     * @param args Normally unused
     */
    public static void main(String[] args) {
        System.out.println("[(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)]");
    }
}
