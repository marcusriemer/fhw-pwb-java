package org.fhw.pwb.ss15;

import java.util.Scanner;

/**
 * A two dimensional field that contains cells with various
 * colours, which are represented using integers. A cell with
 * value 0 is an empty cell.
 * 
 * 
 * @author mri
 */
public class Field {
    
    /**
     * The actual cells of this field. Indexing uses (x,y) coordinates where
     * the point (0,0) is in the lower left corner.
     */
    private int[][] mCells;
    
    /**
     * Height of this field
     */
    private final int mWidth;
    
    /**
     * Width of this field
     */
    private final int mHeight;
    
    /**
     * The opening bracket delimiter of the input format
     */
    private static final String OPENING_BRACKET = "\\[";
    
    /**
     * The closing bracket delimiter of the input format
     */
    private static final String CLOSING_BRACKET = "\\]";
    
    /**
     * 
     * @param input A single line containg a two dimensional list.
     */
    public Field(final String input) {   
        mWidth = Field.getNumCols(input);
        mHeight = Field.getNumRows(input);
        
        // Build internal array of correct size
        mCells = new int[mWidth][mHeight];        
        
        parseInput(input);
    }
    
    /**
     * Takes an input string and reads those values into the already 
     * assigned field.
     * @pre   mCells has correct dimensions
     * @post  mCells is filled with the values that match the input.
     * @param input The original input string.
     */
    private void parseInput(final String input) {
        // Scanners allow easier parsing of text
        Scanner scanner = new Scanner(input);
        
        // First character is the list opener, which we don't need.
        scanner.skip(OPENING_BRACKET);
        
        // Scanning for sublists, each sublist is a row
        for(int y = 0; scanner.hasNext(OPENING_BRACKET + ".*"); y++) {
            // Okay, now we know this is a new list so we don't need the
            // list delimiter anymore.
            scanner.skip(OPENING_BRACKET);
            
            // A sublist must have at least one entry
            mCells[0][y] = scanner.nextInt();
            
            for (int x = 1; !scanner.hasNext(CLOSING_BRACKET + ".*"); x++) {
                scanner.skip(",");
                mCells[x][y] = scanner.nextInt();
            }      
            
            // Each row ends with a closing bracket
            scanner.skip(CLOSING_BRACKET + ".*");
            
            // And may have a comma to separate it from the next row
            if (scanner.hasNext(",.*")) {
                scanner.skip(",");
            }
        }
    }
    
    /**
     * Simulates a "click" at the given position, removing all adjacent
     * cells of the same colour.
     * @param x
     * @param y 
     */
    public void remove(int x, int y) {
        // TODO: Actually remove something
        
        applyGravity();
    }
    
    /**
     * Shifts empty columns to the left and cells with no cell below downwards.
     */
    private void applyGravity() {
        // TODO: Actually apply gravity
    }
    
    /**
     * @return The width of this field.
     */
    public int getWidth() {
        return (mWidth);
    }
    
    /**
     * @return The height of this field.
     */
    public int getHeight() {
        return (mHeight);
    }
    
    /**
     * Retrieves a cell of this field. Every value greater 0 denotes a non empty 
     * cell.
     * @param x Coordinate, indexed from zero
     * @param y Coordinate, indexed from zero
     * @return The value of the cell at (x,y).
     */
    public int getValue(int x, int y) {
        return (mCells[x][y]);
    }
    
    /**
     * Calculates the number of rows from an input string
     * @param input The whole input string, including the outer [ ] delimiters.
     * @return The number of rows
     */
    public static int getNumRows(String input) {
        // Counting the number of opening brackets and subtract one
        // because of the first opening bracket.
        return (input.length() - input.replace("[", "").length() - 1);
    }
    
    /**
     * Calculates the number of columns from an input string
     * @param input The whole input string, including the outer [ ] delimiters.
     * @return The number of columns
     */
    public static int getNumCols(String input) {
        // Extract a string that represents a single column.
        // [[1,2,3,4,5], ...]
        //   \_______/
        //      This is what we want
        String fstCol = input.substring(2, input.indexOf("]"));
        final int numComma = fstCol.length() - fstCol.replace(",", "").length();
        return (numComma + 1);
    }
    
    /**
     * Used to test the correct implementation of a field.
     * @param args 
     */
    public static void main(String[] args) {
        final String oneByOne = "[[0]]";
        final String oneByThree = "[[1],[2],[3]]";
                
        // Making sure the row counts are correct
        assert(Field.getNumRows(oneByOne) == 1);
        assert(Field.getNumRows(oneByThree) == 3);
        
        assert(Field.getNumCols(oneByOne) == 1);
        assert(Field.getNumCols(oneByThree) == 1);
        
        Field fieldOneByOne = new Field(oneByOne);
        assert(fieldOneByOne.getWidth() == 1);
        assert(fieldOneByOne.getHeight() == 1);
        assert(fieldOneByOne.getValue(0, 0) == 0);
        
        Field fieldOneByThree = new Field(oneByThree);
        assert(fieldOneByThree.getWidth() == 1);
        assert(fieldOneByThree.getHeight() == 3);
        assert(fieldOneByThree.getValue(0, 0) == 1);
        assert(fieldOneByThree.getValue(0, 1) == 2);
        assert(fieldOneByThree.getValue(0, 2) == 3);
    }
    
    
}
